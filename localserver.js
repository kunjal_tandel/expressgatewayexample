let app  = require('express')();
let bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/check', function (req, res) {
   res.send({msg: 'hello mobiiworld!!', req: req.header});
});

app.post('/hostname', function (req, res) {
   res.send(req.body);
});

app.listen(9090, '0.0.0.0', function() {console.log('server running on 9090')});